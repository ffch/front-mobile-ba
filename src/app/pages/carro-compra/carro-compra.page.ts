import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../../services/local-storage/local-storage.service';
import { UtilsService } from '../../services/utils/utils.service';
import { config } from '../../../environments/environment';
import { ProductoService } from '../../services/producto/producto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-carro-compra',
  templateUrl: './carro-compra.page.html',
  styleUrls: ['./carro-compra.page.scss'],
})
export class CarroCompraPage implements OnInit {

  public listaProductos:any[] = [];
  public bodegaURL:string = config.API_BASE_URL_BODEGA;
  public anwoURL:string = config.API_BASE_URL_ANWO;
  public total:number = 0;

  constructor(
    private utils:UtilsService, 
    private localStorageServices:LocalStorageService,
    private productoService:ProductoService,
    private router:Router) { }

  ngOnInit() {
    this.listarProductos();
  }

  async listarProductos(){
    //this.utils.mostrarSpinner();
    //this.listaProductos = JSON.parse(localStorage.getItem("carrito")) ;
    this.listaProductos = await this.localStorageServices.obtenerProductosStorage();
    console.log('lista de productos', this.listaProductos);
    if(this.listaProductos){
      this.listaProductos.forEach(pro => {
        pro.sinStock = false;
      });
    }else{
      this.utils.ocultarSpinner();
      return 
    }

    /* this.dsProductos = this.listaProductos;

    if(!this.dsProductos){
      this.dsProductos = [];
    } */
    //this.calcularTotal(this.dsProductos);
    this.calcularTotal(this.listaProductos);
    //this.utils.ocultarSpinner();
  }

  calcularTotal(productos){
    this.total = 0;
    productos.forEach(element => {
      this.total = this.total + element.precio * element.cantidadCarrito;
      
    });
  }

  async quitarProducto(producto){
    //this.listaProductos = JSON.parse(localStorage.getItem("carrito"));
    this.listaProductos = await this.localStorageServices.obtenerProductosStorage();
    let productoObtenido = this.listaProductos.find(x => x._id == producto._id)
    this.utils.restarProductosCarrito$.emit(Number(productoObtenido.cantidadCarrito));
    let i = this.listaProductos.indexOf(productoObtenido);
    this.listaProductos.splice( i, 1);
    //localStorage.setItem( "carrito", JSON.stringify(this.listaProductos) );
    this.localStorageServices.guardarProducto(this.listaProductos);
    this.listarProductos();
  }

  reducirCantidad(cant,producto){
    if( cant.value > 1){
      cant.value = Number(cant.value) - 1;
      producto.sinStock = false;
      this.utils.restarProductosCarrito$.emit(1);
    }
    producto.cantidadCarrito = cant.value;
    this.calcularTotal(this.listaProductos);
    this.guardarCarrito();
  }

  aumentarCantidad(cant,producto){
    if( cant.value < producto.stock){
      cant.value = Number(cant.value) + 1;
      this.utils.sumarProductosCarrito$.emit(1);
      if ( cant.value == producto.stock){
        producto.sinStock = true;
      }
    } 
    producto.cantidadCarrito = cant.value;
    
    this.calcularTotal(this.listaProductos);
    this.guardarCarrito();
  }

  async generarCompra(){
    this.utils.mostrarSpinner();
    //this.listaProductos = JSON.parse(localStorage.getItem("carrito"));
    this.listaProductos = await this.localStorageServices.obtenerProductosStorage();
    console.log('lista desde local storage', this.listaProductos);

    this.listaProductos.forEach(producto => {

      producto.stock = producto.stock - producto.cantidadCarrito;

      this.productoService.actualizarProducto(producto).subscribe( data => {

        if(data.status == 201){

          this.utils.mensajeExitoso('Pago realizado con éxito');
          this.resetearCarrito();
          this.utils.ocultarSpinner();
        }
      }, error => {
        console.log(error);
        this.utils.mensajeError("Error al actualizar producto");
        this.utils.ocultarSpinner();
      });
      
    });
    
  }

  guardarCarrito(){
    this.localStorageServices.guardarProducto(this.listaProductos);
  }

  resetearCarrito(){

    this.localStorageServices.limpiarStorage();
    this.utils.resetearCantidadProductosCarro$.emit(0);
    this.utils.hayVenta$.emit(true);
    this.listarProductos();

  }

}
