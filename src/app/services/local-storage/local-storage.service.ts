import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  public productosStorage:any[] = [];

  constructor(private storage:Storage) { }

  guardarProducto(producto){

    //this.productosStorage.push(producto);

    this.storage.set('carrito', producto);

  }

  async obtenerProductosStorage():Promise<any>{

    return await this.storage.get('carrito');
    
  }

  limpiarStorage(){

    this.storage.clear();
  }
}
