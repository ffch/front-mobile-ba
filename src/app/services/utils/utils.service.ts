import { Injectable, EventEmitter } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { NgxSpinnerService } from "ngx-spinner";

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  cargando:any;
  toast:any;
  public sumarProductosCarrito$ = new EventEmitter<Number>();
  public restarProductosCarrito$ = new EventEmitter<Number>();
  public resetearCantidadProductosCarro$ = new EventEmitter<Number>();
  public hayVenta$ = new EventEmitter<Boolean>();

  constructor(
    private loadingController: LoadingController,
    private toastController: ToastController,
    private spinner:NgxSpinnerService) { }

  async mostrarSpinner() {
    /* this.cargando = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Cargando...',
      //duration: 2000
    });
    return await this.cargando.present(); */
    this.spinner.show(undefined, {
      type: "ball-square-clockwise-spin",
      size: "medium",
      bdColor: "rgba(0, 0, 0, 0.8)",
      color: "white"
    });
  }

  async ocultarSpinner(){
    this.spinner.hide();
    /* this.cargando.dismiss(); */
  }

  async mensajeExitoso(mensaje:string){

    this.toast = await this.toastController.create({
      message: mensaje,
      duration: 2000,
      color: 'success'
    });
    return await this.toast.present();
  }

  async mensajeError(mensaje:string){

    this.toast = await this.toastController.create({
      message: mensaje,
      duration: 2000,
      color: 'danger'
    });
    return await this.toast.present();
  }
}
