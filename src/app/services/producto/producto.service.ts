import { Injectable } from '@angular/core';
import { config } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Producto } from '../../models/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private http:HttpClient) { }

  obtenerProductosDesdeBodegaYANWO(): Observable<any>{

    return this.http.get( config.API_BASE_URL_BODEGA + 'productos-vigentes');
  }

  actualizarProducto(producto:Producto):Observable<any>{

    return this.http.put( config.API_BASE_URL_BODEGA + 'productos/' + producto._id , producto, {observe: 'response'});
  }
}
