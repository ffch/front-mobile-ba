import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarroCompraPageRoutingModule } from './carro-compra-routing.module';

import { CarroCompraPage } from './carro-compra.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarroCompraPageRoutingModule,
    ComponentsModule
  ],
  declarations: [CarroCompraPage]
})
export class CarroCompraPageModule {}
