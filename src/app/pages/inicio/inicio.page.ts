import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductoService } from '../../services/producto/producto.service';
import { config } from '../../../environments/environment';
import { UtilsService } from '../../services/utils/utils.service';
import { LocalStorageService } from '../../services/local-storage/local-storage.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  public productos:any;
  public bodegaURL:string = config.API_BASE_URL_BODEGA;
  public anwoURL:string = config.API_BASE_URL_ANWO;
  public productosEnCarrito:number = 0;
  public hayVenta:boolean = false;

  constructor(
    private router:Router, 
    private productoService:ProductoService,
    private utils:UtilsService,
    private localStorageService:LocalStorageService) { }

  ngOnInit() {

    this.utils.sumarProductosCarrito$.subscribe( data => {

      this.productosEnCarrito += data;
    });

    this.utils.restarProductosCarrito$.subscribe( data => {

      this.productosEnCarrito -= data;
    });

    this.utils.resetearCantidadProductosCarro$.subscribe( data => {

      this.productosEnCarrito = data;
    });

    this.listarProductos();
  }

  irHaciaCarrito(){
    this.router.navigate(['carro-compra']);
  }

  reducirCantidad(cant,producto){
    
    if( cant.value > 1){
      cant.value = Number(cant.value) - 1;
      producto.sinStock = false;
      if(cant.value == 1){
        producto.esUno = true;
      }else{
        producto.esUno = false;
      }
      
    }else{
      producto.esUno = true;
    }
  }

  async aumentarCantidad(cant,producto){
    cant.value = Number(cant.value) + 1;
    if(cant.value == 1){
      producto.esUno = true;
    }else{
      producto.esUno = false;
    }
    await this.validarStock(producto, cant);
  }

  async validarStock(producto, cant){

    let cantidad: number = Number(cant.value);
    let cantidadCarrito: number = await this.obtenerCantidadCarritoProducto(producto);
    let cantidadTotal: number = cantidad + cantidadCarrito;
    let stock: number = Number(producto.stock);

    if (cantidadTotal >= stock){
      producto.sinStock = true;
    }
    if (cantidadTotal > stock){
      cant.value = Number(stock - cantidadCarrito)
      if(cant.value == 0){
        cant.value = 1;
      }
    }
  }

  async obtenerCantidadCarritoProducto(producto){
    /* let productoLStorage = await this.buscarProducto(producto._id);
    let cantidadCarrito: number;
    if(productoLStorage){
      cantidadCarrito = Number(productoLStorage.cantidadCarrito);
    }else{
      cantidadCarrito = 0;
    }
    return Number(cantidadCarrito); */

    let productoLocalStorage = await this.buscarProducto(producto._id);

    let cantidadCarrito: number;

    if(productoLocalStorage){
      cantidadCarrito = Number(productoLocalStorage.cantidadCarrito);
      
    }else{
      cantidadCarrito = 0;
    }
    return Number(cantidadCarrito);
  }

  async buscarProducto(_id){
    
    /* let listaObtenida = JSON.parse(localStorage.getItem("carrito"));
    if(!listaObtenida) return null;
    let productoObtenido = listaObtenida.find(x => x._id == _id) 
    return productoObtenido; */

    let listaProductosLocalStorage = await this.localStorageService.obtenerProductosStorage();

    if(!listaProductosLocalStorage) return null;

    let productoObtenido = listaProductosLocalStorage.find(x => x._id == _id);

    return productoObtenido;

  }

  async agregarProducto(producto, cant){
    /* let pros;
    console.log('PRODUCTO AGREGAGO', producto);
    console.log('CANTIDAD', cant.value);
    this.localStorageService.guardarProducto(producto);

    pros = await this.localStorageService.obtenerProductosStorage();
    
    console.log('productos storage', pros); */

    let cantidadCarrito = await this.obtenerCantidadCarritoProducto(producto);

    let cantidadTotal: number;
    cantidadTotal = Number(cant.value) + Number(cantidadCarrito);
    await this.validarStock(producto, cant);

    if(producto.stock >= cantidadTotal){

      producto.cantidadCarrito = 0;
      producto.cantidadCarrito = cant.value;
      this.utils.sumarProductosCarrito$.emit( Number(cant.value));

      let listaNueva = [];

      //let listaObtenida = JSON.parse(localStorage.getItem("carrito"));
      let listaObtenida = await this.localStorageService.obtenerProductosStorage();

      if(listaObtenida){

        if(await this.buscarProducto(producto._id) == null ){
          listaObtenida.push(producto);
          //localStorage.setItem("carrito", JSON.stringify(listaObtenida));
          this.localStorageService.guardarProducto(listaObtenida);
        }else{
          let productoEncontrado = await this.buscarProducto(producto._id);
          productoEncontrado.cantidadCarrito = Number(productoEncontrado.cantidadCarrito) + Number(cant.value);
          
          await this.quitarProducto(productoEncontrado);
          //let listaPorActualizar = JSON.parse(localStorage.getItem("carrito"));
          let listaPorActualizar = await this.localStorageService.obtenerProductosStorage();
          listaPorActualizar.push(productoEncontrado);
          //localStorage.setItem("carrito", JSON.stringify(listaPorActualizar));
          this.localStorageService.guardarProducto(listaPorActualizar);
        }
        
      }else{
        listaNueva.push(producto);
        //localStorage.setItem("carrito", JSON.stringify(listaNueva) );
        this.localStorageService.guardarProducto(listaNueva);
      }

      this.utils.mensajeExitoso("Agregó " + cant.value + " " + producto.nombre + " al carrito de compras");
      
    }else{
      this.utils.mensajeError("La cantidad seleccionada supera el stock del producto");
      
    }
  }

  async quitarProducto(producto){

    //let listaObtenida = JSON.parse(localStorage.getItem("carrito"));
    let listaObtenida = await this.localStorageService.obtenerProductosStorage();
    let productoObtenido = listaObtenida.find(x => x._id == producto._id)
    let i = listaObtenida.indexOf(productoObtenido);
    listaObtenida.splice( i, 1);
    //localStorage.setItem( "carrito", JSON.stringify(listaObtenida) );
    this.localStorageService.guardarProducto(listaObtenida);
  }

  listarProductos(){
    this.utils.mostrarSpinner();
    this.productoService.obtenerProductosDesdeBodegaYANWO().subscribe( data => {

      this.productos = data['productosVigentes'];
      this.productos.forEach(producto => {
        if(producto.stock == 0){
          producto.sinStock = true;
          producto.sinStockBD = true;
        }else{
          producto.sinStock = false;
          producto.sinStockBD = false;
        }
        producto.esUno = true;
      });
      console.log('PRODUCTOS BODEGA Y ANWO', this.productos);

      this.utils.ocultarSpinner();
    }, error => {
      console.log(error);
      this.utils.ocultarSpinner();
    }); 
  }
  /* ionViewDidLoad() {
    console.log('ionVewDidLoad');
  } */ 
  ionViewWillEnter(){
    this.utils.hayVenta$.subscribe( data => {

      this.hayVenta = data;
    });
    if(this.hayVenta){
      this.listarProductos();
    }
  }
  /* ionViewDidLeave(){
    console.log('ionViewDidLeave');
  } */
}
